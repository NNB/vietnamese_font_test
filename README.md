<img width="100%" src="https://capsule-render.vercel.app/api?type=waving&section=header&color=0284C7&fontColor=F0F9FF&height=256&text=Vietnamese%20Font%20Test&desc=Unicode%20test%20for%20Vietnamese&fontAlignY=40" />

```
a á à ả ã ạ   o ó ò ỏ õ ọ   e é è ẻ ẽ ẹ   u ú ù ủ ũ ụ
A Á À Ả Ã Ạ   O Ó Ò Ỏ Õ Ọ   E É È Ẻ Ẽ Ẹ   U Ú Ù Ủ ũ Ụ

ă ắ ằ ẳ ẵ ặ   ô ố ồ ổ ỗ ộ   ê ế ề ể ễ ệ   ư ứ ừ ử ữ ự
Ă Ắ Ằ Ẳ Ẵ Ặ   Ô Ố Ồ Ổ Ỗ Ộ   Ê Ế Ề Ể Ễ Ệ   Ư Ứ Ừ Ử Ữ Ự

â ấ ầ ẩ ẫ ậ   ơ ớ ờ ở ỡ ợ   i í ì ỉ ĩ ị   y ỳ ý ỷ ỹ ỵ   đ
Â Ấ Ầ Ẩ Ẫ Ậ   Ơ Ớ Ờ Ở Ỡ Ợ   I Í Ì Ỉ Ĩ Ị   Y Ỳ Ý Ỷ Ỹ Ỵ   Đ
```

## 💌 Credits

Special thanks to:
- [**Vietnamese Comprehensive Unicode Test Page**](https://www.wazu.jp/gallery/Test_Vietnamese.html) from [WAZU JAPAN's Gallery of Unicode Fonts](https://www.wazu.jp/index.html)

<a href="https://codeberg.org/NNB">
  <img
    width="100%"
    src="https://capsule-render.vercel.app/api?type=waving&section=footer&color=0284C7&fontColor=F0F9FF&height=128&desc=Made%20with%20%26lt;3%20by%20NNB&descAlignY=80"
    alt="Made with <3 by NNB"
  />
</a>
